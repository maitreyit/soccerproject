package comm.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class League {
    private String leagueId;
    private String title;
    private String year;
    private String season;
}


